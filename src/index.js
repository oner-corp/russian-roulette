// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import {MiniApp} from 'aq-miniapp';
import View from './views/View';
import './index.css';

function generateShouldWin() {
  return Math.random() > 0.5;
}

ReactDOM.render(
  <MiniApp
    join={View}
    data={{
      shouldWin: generateShouldWin()
    }}
    devt={true}
  />,
  document.getElementById('root')
);
