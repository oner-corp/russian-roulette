// @flow
import React, {Component} from 'react';
import type {Output} from '../Types';
import '../css/View2.css';
import {GameComponent, GameStateRunning} from "../components/GameComponent";

export type Props = {
  onClick: (Output) => void,
  shouldWin: boolean
};

export class View2 extends Component {
  state: {
    output: Output
  };

  constructor(props: Props) {
    super(props);

    this.state = {
      output: {}
    }
  }

  render() {
    return (
      <div>
        <GameComponent/>
      </div>
    )
  }
}
