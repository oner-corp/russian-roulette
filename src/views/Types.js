// @flow
import type {GameState} from "./components/GameComponent";

export type Output = {
  gameState: GameState
}
