// @flow
import React, {Component} from 'react';
import {CloudStorage} from 'aq-miniapp';

import type {Output} from './Types';
import {GameComponent} from "./components/GameComponent";

type Props = {
  cloudStorageClient: CloudStorage,
  id?: string,
  data?: Object,
  mode: 'preview' | 'join'
}

export default class View extends Component {
  state: {
    currentPage: number,
    output: Output,
    data: ?Object,
    mode: 'preview' | 'join',
  }

  constructor(props: Props) {
    super(props);
    this.state = {
      currentPage: 1,
      output: {},
      data: props.data,
      mode: props.mode
    }
  }

  render() {
    return (
      <div className='container' style={{backgroundColor: "#000000"}}>
        <GameComponent calculateGameResults={() => this.props.data.shouldWin}/>
      </div>
    );
  }
}
