// @flow
import '../css/GameComponentCss.css';
import React, {Component} from 'react';
import * as Pixi from 'pixi.js';
import {sound as PixiSound} from 'pixi-sound';
import {VectorUtils} from '../../utils/VectorUtils';
import bangImg from '../../assets/bang.png'
import bgImg from '../../assets/bg.jpg'
import bulletImg from '../../assets/bullet.png'
import clickImg from '../../assets/click.png'
import cylinderImg from '../../assets/cylinder.png'
import faceImg from '../../assets/face.png'
import clickFaceImg from '../../assets/click_face.png'
import gameOverFaceImg from '../../assets/game_over_face.png'
import gunImg from '../../assets/gun.png'
import loadedBulletImg from '../../assets/loaded_bullet.png'
import gunFireMp3 from '../../assets/gun_fire.mp3'
import loseSoundMp3 from '../../assets/lose_sound.mp3'
import missFireMp3 from '../../assets/miss_fire.mp3'
import winSoundMp3 from '../../assets/win_sound.mp3'
import loadBulletSoundMp3 from '../../assets/load_bullet.mp3'
import spinCylinderSoundMp3 from '../../assets/spin_revolver.mp3'
import triggerMovingPartImg from '../../assets/trigger_moving.png'
import triggerStaticPartImg from '../../assets/trigger_static.png'
import heartImg from '../../assets/heart.png'
import laughSoundMp3 from '../../assets/laugh_emoji.mp3'

const WHITE = 0xFFFFFF;
const CYLINDER_BG_RADIUS = 95;
const BLACK = 0x000000;
const ANCHOR_CENTER = 0.5;
const SEMITRANSPARENT = 0.5;
const GREEN = 0x00FF00;
const OOOPS_TRY_AGAIN_COLOR = 0xFF0000;
const ALMOST_EQUAL_DELTA = 1e-3;

const LOAD_BULLETS_FONT_SIZE = 30;
const LOAD_BULLETS_TEXT = 'LOAD THE BULLETS';

const LUCKY_DAY_FONT_SIZE = 50;
const LUCKY_DAY_TEXT = 'Lucky day!';

const SWIPE_TO_FIRE_FONT_SIZE = 30;
const SWIPE_TO_FIRE_TEXT = 'SWIPE TO FIRE';

const BULLETS_CONTAINER_BOTTOM_OFFSET = 100;
const LOAD_BULLETS_TEXT_TOP_OFFSET = 60;
const YOU_HAVE_TEXT_FONT_SIZE = 28;
const ACTION_BUTTON_FONT_SIZE = 34;
const TEXT_SHADOW_ANGLE = Math.PI / 6;
const TEXT_FONT_FAMILY = 'Canaro Medium';
const TEXT_SHADOW_BLUR = 4;
const TEXT_SHADOW_DISTANCE = 2;
const CLICK_ROTATION = -0.3;
const MOVE_BULLET_TO_CYLINDER_MIN_DISTANCE = 100;
const TRIGGER_MIN_ROTATION = 0.2;
const BULLET_MOVEMENT_RATIO = 15;
const CYLIDER_SPIN_RATIO = 0.01;
const GUN_ROTATION_DELTA = 0.1;
const GUN_MAX_ROTATION = 0.3;

const DIFFICULTY_LEVELS = [6, 6, 5, 5, 4, 3, 2, 2, 1, 1];

const BANG_POSITION_RATIO = {
  x: 30 / 48,
  y: 60 / 128
};
const ACTION_BUTTON_CONTAINER_POSITION_RATIO = {
  x: 1 / 2,
  y: 57 / 64
};
const GUN_POSITION_RATIO = {
  x: 12 / 48,
  y: 67 / 128
};
const CLICK_POSITION_RATIO = {
  x: 13 / 48,
  y: 70 / 128
};
const LUCKY_DAY_TEXT_POSITION_RATIO = {
  x: 1 / 2,
  y: 24 / 128
};
const SWIPE_TO_FIRE_POSITION_RATIO = {
  x: 1 / 2,
  y: 48 / 64
};
const FACE_POSITION_RATIO = {
  x: 2 / 3,
  y: 1 / 2
};
const GAME_OVER_FACE_POSITION_RATIO = {
  x: 2 / 3,
  y: 1 / 2
};
const TRIGGER_PIVOT = {x: -25, y: -50};
const TRIGGER_POSITION_RATIO = {
  x: 1 / 2,
  y: 57 / 64
};
const OOPS_TRY_AGAIN_CONTAINER_POSITION_RATIO = {
  x: 1 / 2,
  y: 24 / 128
};
const OOPS_TEXT_TOP_OFFSET = -30;
const TRY_AGAIN_TOP_OFFSET = 30;
const OOPS_TEXT_FONT_SIZE = 64;
const GAME_OVER_TEXT_FONT_SIZE = 54;
const TRY_AGAIN_TEXT_FONT_SIZE = 40;
const BEST_LUCK_NEXT_TIME_FONT_SIZE = 28;
const CYLINDER_CONTAINER_POSITION_RATIO = {
  x: 1 / 2,
  y: 28 / 64
};

const MAX_DISTANCE_BETWEEN_GUN_AND_FACE = 300;

export type GameComponentProps = {
  calculateGameResults?: () => boolean,
};

export type AnimationState = {
  moveBulletBack?: boolean,
  moveBulletToSlot?: boolean,
  animateWin?: boolean,
  animateLoose?: boolean,
  animateClick?: boolean,
  resultsTimeout?: number,
  spinCylinder?: boolean,
  spinCylinderStep?: number,
  isMissFireSoundPlayed?: boolean,
  isGunFireSoundPlayed?: boolean,
  bullet?: any,
  targetSlot?: any,
  startClickAnimation?: boolean,
  startLoseAnimation?: boolean,
  startLaughSoundPlaying?: boolean,
  startLoseSoundPlaying?: boolean,
  startLaughSoundPlaying?: boolean,
  startWinAnimation?: boolean,
  startWinSoundPlaying?: boolean,
  winSoundStarted?: boolean,
  playBulletLoadedSound?: boolean,
  playCylinderSpinningSound?: boolean,
  stopCylinderSpinngingSound?: boolean,
  showFaceTimeout?: number,
}

export class GameComponent extends Component {
  animationState: AnimationState;
  props: GameComponentProps;

  constructor(props: GameComponentProps) {
    super(props);

    this.app = null;
    this.gameCanvas = null;
    this.bg = null;
    this.gameContainer = null;
    this.resultsContainer = null;
    this.triggerContainer = null;
    this.cylinderContainer = null;
    this.cylinder = null;
    this.bulletsContainer = null;
    this.face = null;
    this.gunFireSound = null;
    this.missFireSound = null;
    this.winSound = null;
    this.loseSound = null;
    this.laughSound = null;
    this.shouldWin = null;
  }

  static generateRandomNumber(startInclusive, endInclusive) {
    return Math.floor((endInclusive + 1 - startInclusive) * Math.random() + startInclusive)
  }

  componentDidMount() {
    Pixi.settings.SCALE_MODE = Pixi.SCALE_MODES.LINEAR;

    this.shouldWin = this.props.calculateGameResults();
    if (this.shouldWin) {
      this.winRoundIndex = GameComponent.generateRandomNumber(1, 3) - 1;
    } else {
      this.winRoundIndex = -1;
    }
    this.currentRoundIndex = null;

    this.app = new Pixi.Application(window.innerWidth, window.innerHeight,
      {antialias: true});
    this.gameCanvas.appendChild(this.app.view);

    this.createBackground();

    this.gameContainer = new Pixi.Container();
    this.resultsContainer = new Pixi.Container();
    this.triggerContainer = new Pixi.Container();
    this.resultsContainer.visible = false;
    this.triggerContainer.visible = false;

    this.app.stage.addChild(this.gameContainer);
    this.app.stage.addChild(this.resultsContainer);
    this.app.stage.addChild(this.triggerContainer);

    this.loadedBulletTexture = Pixi.Texture.fromImage(loadedBulletImg);

    this.createHeartsContainer();
    this.createTopHeartsContainer();
    this.createCylinder();
    this.createBullets();

    this.createActionButton();
    this.createFaceAndGun();

    this.createLuckyDayText();
    this.createTryAgainText();
    this.createGameOverText();

    this.createTrigger();

    this.gameState = {
      bulletsCylinderPositions: null,
      updateBgScale: true,
    };
    this.animationState = {};

    this.app.ticker.add((d) => this.tickHandler(d));
    this.loadSounds();

    this.app.start();
    this.resetGame();
  }

  createFaceAndGun() {
    this.createFace();
    this.createClickFace();
    this.createGameOverFace();
    this.createGun();
    this.createClick();
    this.createBang();

    const horizontalDistance = this.face.x - this.gun.x;
    if (horizontalDistance > MAX_DISTANCE_BETWEEN_GUN_AND_FACE) {
      this.face.x -= (horizontalDistance - MAX_DISTANCE_BETWEEN_GUN_AND_FACE) / 2;
      this.gameOverFace.x -= (horizontalDistance - MAX_DISTANCE_BETWEEN_GUN_AND_FACE) / 2;
      this.clickFace.x -= (horizontalDistance - MAX_DISTANCE_BETWEEN_GUN_AND_FACE) / 2;
      this.bang.x -= (horizontalDistance - MAX_DISTANCE_BETWEEN_GUN_AND_FACE) / 2;

      this.gun.x += (horizontalDistance - MAX_DISTANCE_BETWEEN_GUN_AND_FACE) / 2;
      this.click.x += (horizontalDistance - MAX_DISTANCE_BETWEEN_GUN_AND_FACE) / 2;
    }
  }

  createBackground() {
    this.bg = Pixi.Sprite.fromImage(bgImg);
    this.app.stage.addChild(this.bg);
  }

  loadSounds() {
    this.bulletLoadedSound = PixiSound.Sound.from({url: loadBulletSoundMp3, preload: true});
    this.spinCylinderSoundMp3 = PixiSound.Sound.from({url: spinCylinderSoundMp3, preload: true, loop: true});
    this.gunFireSound = PixiSound.Sound.from({url: gunFireMp3, preload: true});
    this.missFireSound = PixiSound.Sound.from({url: missFireMp3, preload: true});
    this.winSound = PixiSound.Sound.from({url: winSoundMp3, preload: true});
    this.loseSound = PixiSound.Sound.from({url: loseSoundMp3, preload: true});
    this.laughSound = PixiSound.Sound.from({url: laughSoundMp3, preload: true});
  }

  static createTextStyle(color, fontSize, wight?: string) {
    return new Pixi.TextStyle({
      fill: color,
      fontFamily: TEXT_FONT_FAMILY,
      fontSize: fontSize,
      fontWight: wight,
      dropShadow: true,
      dropShadowColor: BLACK,
      dropShadowBlur: TEXT_SHADOW_BLUR,
      dropShadowAngle: TEXT_SHADOW_ANGLE,
      dropShadowDistance: TEXT_SHADOW_DISTANCE
    });
  }

  static setPosition(obj, relatedObj, ratio) {
    obj.x = ratio.x * relatedObj.width;
    obj.y = ratio.y * relatedObj.height;
  }

  static setAnchorAndPosition(obj, relatedObj, ratio) {
    obj.anchor.set(ANCHOR_CENTER);
    GameComponent.setPosition(obj, relatedObj, ratio);
  }

  createBullets() {
    this.bulletsContainer = new Pixi.Container();
    this.bulletsContainer.x = 0;
    this.bulletsContainer.y = this.app.screen.height - BULLETS_CONTAINER_BOTTOM_OFFSET;
    this.gameContainer.addChild(this.bulletsContainer);
    this.updateBullets();
  }

  updateBullets() {
    if (this.shouldWin && this.currentRoundIndex === this.winRoundIndex) {
      this.currentRoundIsWin = true;
      this.totalBulletsCount = DIFFICULTY_LEVELS[GameComponent.generateRandomNumber(1, 5) - 1];
    } else {
      this.currentRoundIsWin = false;
      this.totalBulletsCount = DIFFICULTY_LEVELS[GameComponent.generateRandomNumber(6, 10) - 1];
      while (this.totalBulletsCount === 6) {
        this.totalBulletsCount = DIFFICULTY_LEVELS[GameComponent.generateRandomNumber(6, 10) - 1];
      }
    }

    while (this.bulletsContainer.children.length > 0) {
      this.bulletsContainer.removeChild(this.bulletsContainer.children[0]);
    }

    const bulletTexture = Pixi.Texture.fromImage(bulletImg);
    for (let i = 0; i < this.totalBulletsCount; ++i) {
      const bullet = new Pixi.Sprite(bulletTexture);
      bullet.anchor.set(ANCHOR_CENTER);
      bullet.x = (i + 1) * this.app.screen.width / (this.totalBulletsCount + 1);
      bullet.y = 0;
      bullet.interactive = true;
      bullet.index = i;
      bullet.homeX = bullet.x;
      bullet.homeY = bullet.y;
      bullet
        .on('pointerdown', (e) => this.onBulletDragStart(bullet, e))
        .on('pointerup', (e) => this.onBulletDragEnd(bullet, e))
        .on('pointerupoutside', (e) => this.onBulletDragEnd(bullet, e))
        .on('pointermove', (e) => this.onBulletDragMove(bullet, e));

      this.bulletsContainer.addChild(bullet);
    }

    const loadBulletsStyle = GameComponent.createTextStyle(WHITE, LOAD_BULLETS_FONT_SIZE, 'bold');
    const loadBulletsText = new Pixi.Text(LOAD_BULLETS_TEXT, loadBulletsStyle);
    loadBulletsText.anchor.set(ANCHOR_CENTER);
    loadBulletsText.x = this.app.screen.width / 2;
    loadBulletsText.y = LOAD_BULLETS_TEXT_TOP_OFFSET;
    this.bulletsContainer.addChild(loadBulletsText);
  }

  createBang() {
    this.bang = Pixi.Sprite.fromImage(bangImg);
    GameComponent.setAnchorAndPosition(this.bang, this.app.screen, BANG_POSITION_RATIO);
    this.resultsContainer.addChild(this.bang);
  }

  createGameOverFace() {
    this.gameOverFace = Pixi.Sprite.fromImage(gameOverFaceImg);
    this.gameOverFace.rotation = -0.3;
    GameComponent.setAnchorAndPosition(this.gameOverFace, this.app.screen, GAME_OVER_FACE_POSITION_RATIO);
    this.gameOverFace.visible = false;
    this.resultsContainer.addChild(this.gameOverFace);
  }

  createActionButton() {
    this.actionButtonContainer = new Pixi.Container();
    GameComponent.setPosition(this.actionButtonContainer, this.app.screen, ACTION_BUTTON_CONTAINER_POSITION_RATIO);
    this.actionButtonStyle = GameComponent.createTextStyle(WHITE, ACTION_BUTTON_FONT_SIZE);
    this.actionButtonContainer.visible = false;
    this.app.stage.addChild(this.actionButtonContainer);
  }

  createFace() {
    this.face = Pixi.Sprite.fromImage(faceImg);
    GameComponent.setAnchorAndPosition(this.face, this.app.screen, FACE_POSITION_RATIO);
    this.resultsContainer.addChild(this.face);
  }

  createClickFace() {
    this.clickFace = Pixi.Sprite.fromImage(clickFaceImg);
    GameComponent.setAnchorAndPosition(this.clickFace, this.app.screen, FACE_POSITION_RATIO);
    this.resultsContainer.addChild(this.clickFace);
    this.clickFace.visible = false;
  }

  createGun() {
    this.gun = Pixi.Sprite.fromImage(gunImg);
    GameComponent.setAnchorAndPosition(this.gun, this.app.screen, GUN_POSITION_RATIO);
    this.resultsContainer.addChild(this.gun);
  }

  createClick() {
    this.click = Pixi.Sprite.fromImage(clickImg);
    GameComponent.setAnchorAndPosition(this.click, this.app.screen, CLICK_POSITION_RATIO);
    this.click.rotation = CLICK_ROTATION;
    this.resultsContainer.addChild(this.click);
  }

  createLuckyDayText() {
    const luckyDayStyle = GameComponent.createTextStyle(GREEN, LUCKY_DAY_FONT_SIZE);
    this.luckyDayText = new Pixi.Text(LUCKY_DAY_TEXT, luckyDayStyle);
    GameComponent.setAnchorAndPosition(this.luckyDayText, this.app.screen, LUCKY_DAY_TEXT_POSITION_RATIO);
    this.resultsContainer.addChild(this.luckyDayText);
  }

  createTrigger() {
    const swipeToFireStyle = GameComponent.createTextStyle(WHITE, SWIPE_TO_FIRE_FONT_SIZE, 'bold');
    const swipeToFireText = new Pixi.Text(SWIPE_TO_FIRE_TEXT, swipeToFireStyle);
    GameComponent.setAnchorAndPosition(swipeToFireText, this.app.screen, SWIPE_TO_FIRE_POSITION_RATIO);
    this.triggerContainer.addChild(swipeToFireText);

    this.movingTriggerPart = Pixi.Sprite.fromImage(triggerMovingPartImg);
    GameComponent.setAnchorAndPosition(this.movingTriggerPart, this.app.screen, TRIGGER_POSITION_RATIO);
    this.movingTriggerPart.pivot.set(TRIGGER_PIVOT.x, TRIGGER_PIVOT.y);
    this.movingTriggerPart.x += TRIGGER_PIVOT.x;
    this.movingTriggerPart.y += TRIGGER_PIVOT.y;
    this.triggerContainer.addChild(this.movingTriggerPart);

    this.staticTriggerPart = Pixi.Sprite.fromImage(triggerStaticPartImg);
    GameComponent.setAnchorAndPosition(this.staticTriggerPart, this.app.screen, TRIGGER_POSITION_RATIO);
    this.triggerContainer.addChild(this.staticTriggerPart);

    this.movingTriggerPart.interactive = true;
    this.movingTriggerPart
      .on('pointerdown', (e) => this.onTriggerDragStart(e))
      .on('pointerup', (e) => this.onTriggerDragEnd(e))
      .on('pointerupoutside', (e) => this.onTriggerDragEnd(e))
      .on('pointermove', (e) => this.onTriggerDragMove(e));

  }

  createTryAgainText() {
    const oopsStyle = GameComponent.createTextStyle(OOOPS_TRY_AGAIN_COLOR, OOPS_TEXT_FONT_SIZE);
    const tryAgainStyle = GameComponent.createTextStyle(OOOPS_TRY_AGAIN_COLOR, TRY_AGAIN_TEXT_FONT_SIZE);

    this.oopsTryAgainTextContiner = new Pixi.Container();
    GameComponent.setPosition(this.oopsTryAgainTextContiner, this.app.screen, OOPS_TRY_AGAIN_CONTAINER_POSITION_RATIO);

    const oopsText = new Pixi.Text('Oooopps', oopsStyle);
    oopsText.anchor.set(ANCHOR_CENTER);
    oopsText.y = OOPS_TEXT_TOP_OFFSET;
    this.oopsTryAgainTextContiner.addChild(oopsText);

    const tryAgainText = new Pixi.Text('Try again!', tryAgainStyle);
    tryAgainText.anchor.set(ANCHOR_CENTER);
    tryAgainText.y = TRY_AGAIN_TOP_OFFSET;
    this.oopsTryAgainTextContiner.addChild(tryAgainText);

    this.resultsContainer.addChild(this.oopsTryAgainTextContiner);
  }

  createGameOverText() {
    const gameOverStyle = GameComponent.createTextStyle(OOOPS_TRY_AGAIN_COLOR, GAME_OVER_TEXT_FONT_SIZE);
    const betterLuckNextTimeStyle = GameComponent.createTextStyle(WHITE, BEST_LUCK_NEXT_TIME_FONT_SIZE);

    this.gameOverTextContainer = new Pixi.Container();
    GameComponent.setPosition(this.gameOverTextContainer, this.app.screen, OOPS_TRY_AGAIN_CONTAINER_POSITION_RATIO);

    const gameOverText = new Pixi.Text('Game Over', gameOverStyle);
    gameOverText.anchor.set(ANCHOR_CENTER);
    gameOverText.y = OOPS_TEXT_TOP_OFFSET;
    this.gameOverTextContainer.addChild(gameOverText);

    const betterLuckNextTimeText = new Pixi.Text('Better Luck Next Time', betterLuckNextTimeStyle);
    betterLuckNextTimeText.anchor.set(ANCHOR_CENTER);
    betterLuckNextTimeText.y = TRY_AGAIN_TOP_OFFSET;
    this.gameOverTextContainer.addChild(betterLuckNextTimeText);

    this.resultsContainer.addChild(this.gameOverTextContainer);
  }

  createHeartsContainer() {
    this.heartsContainer = new Pixi.Container();
    const ratio = {x: 0.5, y: 0.2};
    GameComponent.setPosition(this.heartsContainer, this.app.screen, ratio);
    this.gameContainer.addChild(this.heartsContainer);

    const heartTexture = Pixi.Texture.fromImage(heartImg);
    for (let i = 0; i < 3; ++i) {
      const heart = new Pixi.Sprite(heartTexture);
      heart.anchor.set(ANCHOR_CENTER);
      heart.x = 15 + i * 50;
      this.heartsContainer.addChild(heart);
    }

    this.livesTextContainer = new Pixi.Container();
    GameComponent.setPosition(this.livesTextContainer, this.app.screen, {x: 0.5, y: 0.2});
    this.gameContainer.addChild(this.livesTextContainer);

    this.createYouHaveText(1000);
  }

  createYouHaveText(timeout?: number) {
    const youHaveTextStyle = GameComponent.createTextStyle(WHITE, YOU_HAVE_TEXT_FONT_SIZE);
    const youHaveText = new Pixi.Text("You have", youHaveTextStyle);
    youHaveText.anchor.x = 1;
    youHaveText.anchor.y = 0.5;
    youHaveText.x = -15;

    this.livesTextContainer.removeChild(this.livesTextContainer.children[0]);
    this.livesTextContainer.addChild(youHaveText);

    if (timeout != null) {
      setTimeout(() => this.createYouHaveText(null), timeout);
    }
  }

  createTopHeartsContainer() {
    this.topHeartsContainer = new Pixi.Container();
    GameComponent.setPosition(this.topHeartsContainer, this.app.screen, {x: 1, y: 0});
    this.app.stage.addChild(this.topHeartsContainer);

    const heartTexture = Pixi.Texture.fromImage(heartImg);
    for (let i = 0; i < 3; ++i) {
      const heart = new Pixi.Sprite(heartTexture);
      heart.scale.set(0.5);
      heart.anchor.x = 1;
      heart.anchor.y = 0;
      heart.x = -25 * i - 15;
      heart.y = 15;
      this.topHeartsContainer.addChild(heart);
    }

    this.topHeartsContainer.visible = false;
  }

  createCylinder() {
    this.cylinderContainer = new Pixi.Container();
    GameComponent.setPosition(this.cylinderContainer, this.app.screen, CYLINDER_CONTAINER_POSITION_RATIO);
    this.gameContainer.addChild(this.cylinderContainer);

    const cylinderBg = new Pixi.Graphics();
    cylinderBg.clear();
    cylinderBg.beginFill(WHITE, 1);
    cylinderBg.drawCircle(0, 0, CYLINDER_BG_RADIUS);
    this.cylinderContainer.addChild(cylinderBg);

    const cylinderTexture = Pixi.Texture.fromImage(cylinderImg);
    this.cylinder = new Pixi.Sprite(cylinderTexture);
    this.cylinder.anchor.set(ANCHOR_CENTER);
    this.cylinderContainer.addChild(this.cylinder);
  }

  componentWillUnmount() {
    this.app.stop();
  }

  setActionButtonText(text, handler) {
    while (this.actionButtonContainer.children.length) {
      this.actionButtonContainer.removeChild(this.actionButtonContainer.children[0])
    }

    const actionButtonLabel = new Pixi.Text(text, this.actionButtonStyle);
    actionButtonLabel.anchor.set(ANCHOR_CENTER);
    actionButtonLabel.interactive = true;
    actionButtonLabel.buttonMode = true;
    if (handler) {
      actionButtonLabel.on('pointerup', handler);
    }
    this.actionButtonContainer.addChild(actionButtonLabel);
    this.actionButtonContainer.visible = true;
  }

  static getBulletTopLocation(bullet) {
    const global = bullet.getGlobalPosition();
    return {
      x: global.x, y: global.y
    }
  }

  calculateTargetCylinderSlot(bullet) {
    if (this.gameState.bulletsCylinderPositions === null) {
      return null;
    }
    const freeSlots = [];
    for (let i = 0; i < this.gameState.bulletsCylinderPositions.length; ++i) {
      const slot = this.gameState.bulletsCylinderPositions[i];
      if (!slot.isLoaded) {
        freeSlots.push(slot);
      }
    }
    if (freeSlots.length === 0) {
      return null;
    }
    let nearestSlot = freeSlots[0];
    const bulletLocation = GameComponent.getBulletTopLocation(bullet);
    for (let i = 0; i < freeSlots.length; ++i) {
      const slot = freeSlots[i];
      const d = VectorUtils.getDistanceBetweenPoints(bulletLocation, slot.globalPoint);
      if (d < VectorUtils.getDistanceBetweenPoints(bulletLocation, nearestSlot.globalPoint)) {
        nearestSlot = slot;
      }
    }
    const d = VectorUtils.getDistanceBetweenPoints(bulletLocation, nearestSlot.globalPoint);
    if (d > MOVE_BULLET_TO_CYLINDER_MIN_DISTANCE) {
      return null;
    }
    return nearestSlot;
  }

  onBulletDragStart(bullet, event) {
    if (this.gameState.isDragging) {
      return;
    }

    bullet.data = event.data;
    bullet.alpha = SEMITRANSPARENT;
    bullet.isDragging = true;
    this.gameState.isDragging = true;
  }

  onBulletDragEnd(bullet, event) {
    if (!bullet.isDragging) {
      return;
    }
    bullet.alpha = 1;
    bullet.isDragging = false;
    bullet.data = null;

    const slot = this.calculateTargetCylinderSlot(bullet);

    if (slot === null) {
      this.animationState.moveBulletBack = true;
    } else {
      this.animationState.moveBulletToSlot = true;
      this.animationState.playBulletLoadedSound = true;
      this.animationState.targetSlot = slot;
    }
    this.animationState.bullet = bullet;
  }

  onBulletDragMove(bullet) {
    if (!this.gameState.isDragging) {
      return;
    }
    if (!bullet.isDragging) {
      return;
    }

    const newPosition = bullet.data.getLocalPosition(bullet.parent);
    bullet.x = newPosition.x;
    bullet.y = newPosition.y;
  }

  onTriggerDragStart(event) {
    if (this.movingTriggerPart.isDragging) {
      return;
    }
    this.movingTriggerPart.isDragging = true;
    let global = event.data.global;
    this.movingTriggerPart.startDraggingPoint = {
      x: global.x, y: global.y
    };
  }

  onTriggerDragMove(event) {
    if (!this.movingTriggerPart.isDragging) {
      return;
    }

    const newPosition = event.data.global;
    const v2 = VectorUtils.toVector(this.movingTriggerPart.startDraggingPoint, newPosition);
    const v1 = {x: VectorUtils.getLength(v2), y: 0};
    const rotationAngle = VectorUtils.getAngleBetweenVectors(v1, v2);

    const triggerRotationAngle = (Math.abs(rotationAngle) - Math.PI / 2) / 3;
    if (triggerRotationAngle > 0) {
      this.movingTriggerPart.rotation = triggerRotationAngle;
    } else {
      this.movingTriggerPart.rotation = 0;
    }
  }

  onTriggerDragEnd(event) {
    if (!this.movingTriggerPart.isDragging) {
      return;
    }

    this.movingTriggerPart.isDragging = false;
    this.movingTriggerPart.startDraggingPoint = null;
    if (this.movingTriggerPart.rotation > TRIGGER_MIN_ROTATION) {
      this.fireHandler();
    } else {
      this.movingTriggerPart.rotation = 0;
    }
  }

  static almostEqual(a, b, delta) {
    let finalDelta;
    if (delta == null) {
      finalDelta = ALMOST_EQUAL_DELTA;
    } else {
      finalDelta = delta;
    }
    return Math.abs(a - b) < finalDelta;
  }

  static calculateNextMovingPointDelta(pointComponent, targetPointComponent, delta) {
    if (GameComponent.almostEqual(pointComponent, targetPointComponent, delta)) {
      return targetPointComponent - pointComponent
    } else if (pointComponent > targetPointComponent) {
      return -delta;
    } else {
      return delta;
    }
  }

  static calculateNextMovingPoint(pointComponent, targetPointComponent, delta) {
    const targetDelta = GameComponent.calculateNextMovingPointDelta(pointComponent, targetPointComponent, delta);
    return pointComponent + targetDelta;
  }

  calculateCylinderSlots() {
    const dAngle = 41 * Math.PI / 128;
    const vectorDistance = 63 * this.cylinder.height / 256;

    const cylinderAngles = [
      0, dAngle, Math.PI - dAngle,
      Math.PI, -dAngle, -Math.PI + dAngle
    ];
    this.gameState.bulletsCylinderPositions = [];
    for (let i = 0; i < cylinderAngles.length; ++i) {
      const angle = cylinderAngles[i];
      const startPoint = {x: 0, y: 0};
      const endPoint = {x: 0, y: -vectorDistance};
      const v = VectorUtils.toVector(startPoint, endPoint);
      const targetV = VectorUtils.rotateVector(v, angle);

      const globalCenter = {
        x: this.cylinderContainer.x,
        y: this.cylinderContainer.y,
      };
      const localPoint = {x: targetV.x, y: targetV.y};
      const targetPoint = VectorUtils.movePointByVector(globalCenter, targetV);

      this.gameState.bulletsCylinderPositions.push({
        localPoint: localPoint,
        globalPoint: targetPoint
      });
    }
  }

  getLoadedBulletsCount(): number {
    if (this.gameState.bulletsCylinderPositions === null) {
      return 0;
    }
    let cnt = 0;
    for (let i = 0; i < this.gameState.bulletsCylinderPositions.length; ++i) {
      const slot = this.gameState.bulletsCylinderPositions[i];
      if (slot.isLoaded) {
        cnt += 1;
      }
    }
    return cnt;
  }

  loadBulletToSlot(bullet, slot) {
    slot.isLoaded = true;

    const loadedBullet = new Pixi.Sprite(this.loadedBulletTexture);
    loadedBullet.anchor.set(ANCHOR_CENTER);
    loadedBullet.position.x = slot.localPoint.x;
    loadedBullet.position.y = slot.localPoint.y;
    this.cylinderContainer.addChild(loadedBullet);
    slot.loadedBullet = loadedBullet;

    bullet.visible = false;
    bullet.interactive = false;

    if (this.getLoadedBulletsCount() === this.totalBulletsCount) {
      const that = this;
      setTimeout(() => {
        that.bulletsContainer.visible = false;
        that.startCylinderSpinning();
      }, 1000);
    }
  }

  fireHandler() {
    const shouldWin = this.currentRoundIsWin;
    const shouldClick = !shouldWin && this.currentRoundIndex < 2;
    const shouldLoose = !shouldWin && this.currentRoundIndex >= 2;

    this.gameContainer.visible = false;
    this.triggerContainer.visible = false;

    this.click.visible = false;
    this.luckyDayText.visible = false;
    this.bang.visible = false;
    this.oopsTryAgainTextContiner.visible = false;
    this.gameOverTextContainer.visible = false;
    this.gun.rotation = 0;
    this.click.scale.x = 0;
    this.click.scale.y = 0;
    this.bang.scale.x = 0;
    this.bang.scale.y = 0;

    this.gameOverFace.visible = false;
    this.face.visible = false;
    this.clickFace.visible = false;

    if (shouldWin) {
      this.animationState.animateWin = true;
      this.animationState.resultsTimeout = GameComponent.createTimeout(300);
      this.face.visible = true;
    } else if (shouldClick) {
      this.animationState.animateClick = true;
      this.animationState.resultsTimeout = GameComponent.createTimeout(500);
      this.clickFace.visible = true;
    } else if (shouldLoose) {
      this.animationState.animateLoose = true;
      this.animationState.resultsTimeout = GameComponent.createTimeout(500);
      this.gameOverFace.visible = true;
    }

    this.resultsContainer.visible = true;
    if (shouldWin || shouldLoose) {
      this.setActionButtonText('NEXT', () => this.resetGame());
    } else {
      this.setActionButtonText('RETRY', () => this.retry());
    }
  }

  startGame() {
    this.resultsContainer.visible = false;
    this.gameContainer.visible = true;
    this.bulletsContainer.visible = true;
    this.heartsContainer.visible = false;
    this.livesTextContainer.visible = false;
    this.topHeartsContainer.visible = true;

    for (let i = 0; i < this.totalBulletsCount; ++i) {
      const bullet = this.bulletsContainer.children[i];
      bullet.x = bullet.homeX;
      bullet.y = bullet.homeY;
      bullet.interactive = true;
      bullet.visible = true;
    }

    for (let i = 0; i < this.gameState.bulletsCylinderPositions.length; ++i) {
      const slot = this.gameState.bulletsCylinderPositions[i];
      slot.isLoaded = false;
      if (slot.loadedBullet) {
        this.cylinderContainer.removeChild(slot.loadedBullet);
      }
    }

    this.cylinderContainer.rotation = 0;

    this.gameState = {
      bulletsCylinderPositions: null,
    };
    this.animationState = {};

    this.setActionButtonText('');
  }

  resetGame() {
    this.currentRoundIndex = 0;
    if (this.shouldWin) {
      this.winRoundIndex = GameComponent.generateRandomNumber(1, 3) - 1;
    } else {
      this.winRoundIndex = -1;
    }
    this.currentRoundIsWin = false;
    this.updateBullets();
    this.currentLives = 3;
    this.resultsContainer.visible = false;
    this.gameContainer.visible = true;
    this.bulletsContainer.visible = false;
    this.heartsContainer.visible = true;
    this.livesTextContainer.visible = true;
    this.topHeartsContainer.visible = false;
    this.animationState.startLoseAnimation = false;
    this.animationState.startLoseSoundPlaying = false;
    this.animationState.startClickAnimation = false;
    this.animationState.startLaughSoundPlaying = false;
    this.animationState.startWinAnimation = false;
    this.animationState.startWinSoundPlaying = false;
    this.animationState.isMissFireSoundPlayed = false;

    this.face.visible = true;
    this.clickFace.visible = false;
    this.gameOverFace.visible = false;

    this.cylinderContainer.rotation = 0;

    if (this.gameState.bulletsCylinderPositions != null) {
      for (let i = 0; i < this.gameState.bulletsCylinderPositions.length; ++i) {
        const slot = this.gameState.bulletsCylinderPositions[i];
        slot.isLoaded = false;
        if (slot.loadedBullet) {
          this.cylinderContainer.removeChild(slot.loadedBullet);
        }
      }
    }

    this.gameState = {
      bulletsCylinderPositions: null,
      updateBgScale: true,
    };
    this.animationState = {};

    for (let i = 0; i < this.heartsContainer.children.length; ++i) {
      this.heartsContainer.children[i].visible = true;
    }
    for (let i = 0; i < this.topHeartsContainer.children.length; ++i) {
      this.topHeartsContainer.children[i].visible = true;
    }

    this.setActionButtonText('TAP TO START', () => this.startGame());
  }

  retry() {
    this.currentRoundIndex += 1;
    this.currentLives -= 1;
    this.resultsContainer.visible = false;
    this.gameContainer.visible = true;
    this.bulletsContainer.visible = false;
    this.heartsContainer.visible = true;
    this.livesTextContainer.visible = true;
    this.topHeartsContainer.visible = false;
    this.animationState.startLoseAnimation = false;
    this.animationState.startLoseSoundPlaying = false;
    this.animationState.startClickAnimation = false;
    this.animationState.startLaughSoundPlaying = false;
    this.animationState.startWinAnimation = false;
    this.animationState.startWinSoundPlaying = false;
    this.animationState.isMissFireSoundPlayed = false;

    this.face.visible = true;
    this.clickFace.visible = false;
    this.gameOverFace.visible = false;

    this.cylinderContainer.rotation = 0;

    if (this.gameState.bulletsCylinderPositions != null) {
      for (let i = 0; i < this.gameState.bulletsCylinderPositions.length; ++i) {
        const slot = this.gameState.bulletsCylinderPositions[i];
        slot.isLoaded = false;
        if (slot.loadedBullet) {
          this.cylinderContainer.removeChild(slot.loadedBullet);
        }
      }
    }

    this.gameState = {
      bulletsCylinderPositions: null,
      updateBgScale: true,
    };
    this.animationState = {};

    for (let i = 0; i < this.heartsContainer.children.length; ++i) {
      this.heartsContainer.children[i].visible = i < this.currentLives;
    }
    for (let i = 0; i < this.topHeartsContainer.children.length; ++i) {
      this.topHeartsContainer.children[i].visible = i < this.currentLives;
    }

    this.updateBullets();

    this.setActionButtonText('TAP TO START', () => this.startGame());
  }

  startCylinderSpinning() {
    this.animationState.spinCylinder = true;
    this.animationState.spinCylinderStep = 0;
    this.animationState.playCylinderSpinningSound = true;
    this.animationState.showFaceTimeout = GameComponent.createTimeout(2500);
    this.setActionButtonText('');
  }

  showFaceHandler() {
    this.gameContainer.visible = false;
    this.animationState.startLoseAnimation = false;
    this.animationState.startLoseSoundPlaying = false;
    this.animationState.startWinAnimation = false;
    this.animationState.startWinSoundPlaying = false;

    this.gun.rotation = 0;
    this.click.visible = false;
    this.luckyDayText.visible = false;
    this.bang.visible = false;
    this.oopsTryAgainTextContiner.visible = false;
    this.gameOverTextContainer.visible = false;

    this.resultsContainer.visible = true;
    this.animationState.stopCylinderSpinngingSound = true;

    this.movingTriggerPart.rotation = 0;
    this.triggerContainer.visible = true;
  }

  static createTimeout(ms) {
    return new Date().getTime() + ms;
  }

  static getDeltaInMilliseconds(time) {
    const currentTime = new Date().getTime();
    return time - currentTime;
  }

  updateBgScale() {
    const widthScale = this.app.screen.width / this.bg.width;
    const heightScale = this.app.screen.height / this.bg.height;

    if (widthScale <= 1 && heightScale <= 1) {
      return;
    }

    const scale = Math.max(widthScale, heightScale);
    this.bg.scale.x = scale;
    this.bg.scale.y = scale;
  }

  tickHandler(delta) {
    if (this.animationState.moveBulletBack) {
      this.handleMoveBulletBackAnimation(delta);
    }
    if (this.animationState.playBulletLoadedSound) {
      this.bulletLoadedSound.play();
      this.animationState.playBulletLoadedSound = false;
    }
    if (this.animationState.moveBulletToSlot) {
      this.handleMoveBulletToSlotAnimation(delta);
    }

    if (this.animationState.playCylinderSpinningSound) {
      this.spinCylinderSoundMp3.play();
      this.animationState.playCylinderSpinningSound = false;
    }
    if (this.animationState.stopCylinderSpinngingSound) {
      this.spinCylinderSoundMp3.stop();
      this.animationState.stopCylinderSpinngingSound = false;
    }
    if (this.animationState.spinCylinder) {
      const spinDelta = Math.pow(Math.cos(this.animationState.spinCylinderStep / 120), 2) * 0.3 * delta;
      this.cylinderContainer.rotation += spinDelta;
      this.animationState.spinCylinderStep += 1;
    }

    if (this.animationState.animateWin) {
      this.handleWinAnimation(delta);
    }
    if (this.animationState.animateClick) {
      this.handleClickAnimation(delta);
    }
    if (this.animationState.animateLoose) {
      this.handleLooseAnimation(delta);
    }

    if (this.gameState.bulletsCylinderPositions == null && this.cylinder.height > 1) {
      this.calculateCylinderSlots()
    }
    if (this.gameState.updateBgScale && this.bg.height > 1) {
      this.updateBgScale();
      this.gameState.updateBgScale = false;
    }

    if (this.animationState.showFaceTimeout !== null) {
      if (GameComponent.getDeltaInMilliseconds(this.animationState.showFaceTimeout) <= 0) {
        this.showFaceHandler();
        this.animationState.showFaceTimeout = null;
      }
    }
  }

  handleMoveBulletToSlotAnimation(delta) {
    const movementDelta = BULLET_MOVEMENT_RATIO * delta;
    const bullet = this.animationState.bullet;
    const slot = this.animationState.targetSlot;

    const currentPoint = GameComponent.getBulletTopLocation(bullet);
    const targetPoint = slot.globalPoint;

    bullet.x += GameComponent.calculateNextMovingPointDelta(currentPoint.x, targetPoint.x, movementDelta);
    bullet.y += GameComponent.calculateNextMovingPointDelta(currentPoint.y, targetPoint.y, movementDelta);

    const newCurrentPoint = GameComponent.getBulletTopLocation(bullet);

    if (GameComponent.almostEqual(newCurrentPoint.x, targetPoint.x, movementDelta) &&
      GameComponent.almostEqual(newCurrentPoint.y, targetPoint.y, movementDelta)) {
      this.gameState.isDragging = false;
      this.animationState.moveBulletToSlot = false;
      this.loadBulletToSlot(bullet, slot);
    }
  }

  handleMoveBulletBackAnimation(delta) {
    const movementDelta = BULLET_MOVEMENT_RATIO * delta;
    const bullet = this.animationState.bullet;

    const bulletTargetX = bullet.homeX;
    const bulletTargetY = bullet.homeY;

    bullet.x = GameComponent.calculateNextMovingPoint(bullet.x, bulletTargetX, movementDelta);
    bullet.y = GameComponent.calculateNextMovingPoint(bullet.y, bulletTargetY, movementDelta);

    if (GameComponent.almostEqual(bullet.x, bulletTargetX, movementDelta) &&
      GameComponent.almostEqual(bullet.y, bulletTargetY, movementDelta)) {
      bullet.x = bulletTargetX;
      bullet.y = bulletTargetY;
      this.gameState.isDragging = false;
      this.animationState.moveBulletBack = false;
    }
  }

  handleGunFireSoundPlayingProgress(progress) {
    this.animationState.startWinAnimation = true;
  }

  handleGunFireSoundCompleted() {
    this.animationState.startWinSoundPlaying = true;
  }

  handleClickAnimation(delta) {
    if (!this.animationState.isMissFireSoundPlayed) {
      const instance = this.missFireSound.play();
      instance.on('progress', (progress) => this.handleMissFireSoundPlayingProgress(progress));
      instance.on('end', () => this.handleMissFireSoundCompleted());
      this.animationState.isMissFireSoundPlayed = true;
    }

    if (!this.animationState.startClickAnimation &&
      GameComponent.getDeltaInMilliseconds(this.animationState.resultsTimeout) >= 0) {
      return;
    }

    if (this.animationState.startLaughSoundPlaying) {
      this.laughSound.play();
      this.animationState.startLaughSoundPlaying = false;
    }

    this.rotateGunAfterFireOrClick(delta, this.click);
    this.oopsTryAgainTextContiner.visible = true;

    if (this.gun.rotation === -GUN_MAX_ROTATION && this.click.scale.x === 1) {
      this.animationState.startClickAnimation = false;
      this.animationState.animateClick = false;
    }
  }

  handleLooseAnimation(delta) {
    if (!this.animationState.isMissFireSoundPlayed) {
      const instance = this.missFireSound.play();
      instance.on('progress', (progress) => this.handleMissFireGameOverSoundPlayingProgress(progress));
      instance.on('end', () => this.handleMissFireGameOverSoundCompleted());
      this.animationState.isMissFireSoundPlayed = true;
    }

    if (!this.animationState.startLoseAnimation &&
      GameComponent.getDeltaInMilliseconds(this.animationState.resultsTimeout) >= 0) {
      return;
    }

    if (this.animationState.startLoseSoundPlaying) {
      this.loseSound.play();
      this.animationState.startLoseSoundPlaying = false;
    }

    this.rotateGunAfterFireOrClick(delta, this.click);
    this.gameOverTextContainer.visible = true;

    if (this.gun.rotation === -GUN_MAX_ROTATION && this.click.scale.x === 1) {
      this.animationState.startLoseAnimation = false;
      this.animationState.animateLoose = false;
    }
  }

  handleWinAnimation(delta) {
    if (!this.animationState.isGunFireSoundPlayed) {
      const instance = this.gunFireSound.play();
      instance.on('progress', (progress) => this.handleGunFireSoundPlayingProgress(progress));
      instance.on('end', () => this.handleGunFireSoundCompleted());
      this.animationState.isGunFireSoundPlayed = true;
    }

    if (!this.animationState.startWinAnimation &&
      GameComponent.getDeltaInMilliseconds(this.animationState.resultsTimeout) >= 0) {
      return;
    }

    if (this.animationState.startWinSoundPlaying) {
      console.log("Start win sound");
      this.winSound.play();
      this.animationState.startWinSoundPlaying = false;
      this.animationState.winSoundStarted = true;
    }

    this.rotateGunAfterFireOrClick(delta, this.bang);
    this.luckyDayText.visible = true;

    if (this.gun.rotation === -GUN_MAX_ROTATION && this.bang.scale.x === 1) {
      if (this.animationState.winSoundStarted) {
        this.animationState.startWinAnimation = false;
        this.animationState.animateWin = false;
        this.animationState.winSoundStarted = false;
      } else {
        this.animationState.startWinSoundPlaying = true;
      }
    }
  }

  handleMissFireSoundPlayingProgress(progress) {
    this.animationState.startClickAnimation = true;
  }

  handleMissFireSoundCompleted() {
    this.animationState.startLaughSoundPlaying = true;
  }

  handleMissFireGameOverSoundPlayingProgress(progress) {
    this.animationState.startLoseAnimation = true;
  }

  handleMissFireGameOverSoundCompleted() {
    this.animationState.startLoseSoundPlaying = true;
  }

  rotateGunAfterFireOrClick(delta, clickOrBang) {
    if (this.gun.rotation > GUN_MAX_ROTATION) {
      this.gun.rotation -= GUN_ROTATION_DELTA * delta;
    } else {
      this.gun.rotation = -GUN_MAX_ROTATION;
    }
    if (clickOrBang.scale.x < 1) {
      clickOrBang.scale.x += 0.1 * delta;
      clickOrBang.scale.y += 0.1 * delta;
    } else {
      clickOrBang.scale.x = 1;
      clickOrBang.scale.y = 1;
    }
    clickOrBang.visible = true;
  }

  render() {
    const component = this;
    return (
      <div ref={(thisDiv) => {
        component.gameCanvas = thisDiv
      }}/>
    );
  }
}
