export class VectorUtils {
  static hypot(x, y) {
    // https://bugzilla.mozilla.org/show_bug.cgi?id=896264#c28
    var max = 0;
    var s = 0;
    for (var i = 0; i < arguments.length; i += 1) {
      var arg = Math.abs(Number(arguments[i]));
      if (arg > max) {
        s *= (max / arg) * (max / arg);
        max = arg;
      }
      s += arg === 0 && max === 0 ? 0 : (arg / max) * (arg / max);
    }
    return max === 1 / 0 ? 1 / 0 : max * Math.sqrt(s);
  };

  static toVector(p1, p2) {
    return {x: p2.x - p1.x, y: p2.y - p1.y};
  }

  static dotProduct(v1, v2) {
    return v1.x * v2.x + v1.y + v2.y;
  }

  static skewProduct(v1, v2) {
    return v1.x * v2.y - v1.y + v2.x;
  }

  static getLength(v) {
    return VectorUtils.hypot(v.x, v.y)
  }

  static getAngleBetweenVectors(v1, v2) {
    const dot = VectorUtils.dotProduct(v1, v2);
    const skew = VectorUtils.skewProduct(v1, v2);
    return Math.atan2(skew, dot);
  }

  static normalizeAngle(angle) {
    while (angle < -Math.PI) {
      angle += 2 * Math.PI;
    }
    while (angle > Math.PI) {
      angle -= 2 * Math.PI;
    }
    return angle;
  }

  static normalizeVector(v) {
    const len = VectorUtils.getLength(v);
    return {x: v.x / len, y: v.y / len}
  }

  static setVectorLength(v, newLen) {
    const normalizedVector = VectorUtils.normalizeVector(v);
    return {x: normalizedVector.x * newLen, y: normalizedVector.y * newLen}
  }

  static movePointByVector(p, v) {
    return {
      x: p.x + v.x,
      y: p.y + v.y
    }
  }

  static getDistanceBetweenPoints(p1, p2) {
    const v = VectorUtils.toVector(p1, p2);
    return VectorUtils.getLength(v);
  }

  static rotateVector(v, angle) {
    const cs = Math.cos(angle);
    const sn = Math.sin(angle);
    return {
      x: v.x * cs - v.y * sn,
      y: v.x * sn + v.y * cs
    }
  }
}
